package br.ucsal.bes20201.testequalidade.aula05;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ExemploUsoAnotacaoJUnit5Test {

	@BeforeAll
	public static void beforeClass() {
		System.out.println("BeforeAll");
	}

	@BeforeEach
	public void before() {
		System.out.println("	BeforeEach");
	}

	@Test
	@DisplayName("Descri��o do teste 1")
	public void test1() {
		System.out.println("		Test1");
	}

	@Test
	public void test2() {
		System.out.println("		Test2");
	}

	@Test
	@Disabled
	public void test3() {
		System.out.println("		Test3 - est� mensagem n�o deve aparecer!");
	}

	@AfterEach
	public void after() {
		System.out.println("	AfterEach");
	}

	@AfterAll
	public static void afterClass() {
		System.out.println("AfterAll");
	}

}
