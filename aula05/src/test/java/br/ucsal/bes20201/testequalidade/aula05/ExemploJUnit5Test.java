package br.ucsal.bes20201.testequalidade.aula05;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class ExemploJUnit5Test {

	@Test
	public void testeMultiplosAssertsSequencial() {
		assertEquals(1, CalculoUtil.calcularFatorial(0));
		assertEquals(1, CalculoUtil.calcularFatorial(1));
		assertEquals(6, CalculoUtil.calcularFatorial(3));
		assertEquals(24, CalculoUtil.calcularFatorial(4));
	}
	
	@Test
	public void testeMultiplosAsserts() {
		assertAll("fatoriais", () -> assertEquals(1, CalculoUtil.calcularFatorial(0)),
				() -> assertEquals(1, CalculoUtil.calcularFatorial(1)),
				() -> assertEquals(6, CalculoUtil.calcularFatorial(3)),
				() -> assertEquals(24, CalculoUtil.calcularFatorial(4)));

	}

	@Test
	public void teste1() {
		Integer esperado1 = 20;
		Integer atual1 = 21;
		assertEquals(esperado1, atual1, "Falha geral");
	}

	@Test
	public void testeComLambda() {
		assertTrue(Stream.of(10, 8).mapToInt(i -> i).sum() == 18, () -> "A soma deve ser 8.");
	}

}
