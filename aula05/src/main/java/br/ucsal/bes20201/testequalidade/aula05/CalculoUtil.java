package br.ucsal.bes20201.testequalidade.aula05;

import java.util.Scanner;

public class CalculoUtil {

	public static int obterNumero() {
		Scanner sc = new Scanner(System.in);
		int n;
		do {
			System.out.println("Informe um n�mero entre 0 e 10:");
			n = sc.nextInt();
			if (n < 0 || n > 10) {
				System.out.println("N�mero fora da faixa.");
			} else {
				return n;
			}
		} while (true);
	}

	public static void exibirFatorial(int n, long fat) {
		System.out.println("Fatorial(" + n + ")=" + fat);
	}

	public static long calcularFatorial(int n) {
		long fat = 1;
		for (int i = 1; i < n; i++) {
			fat *= i;
		}
		return fat;
	}

}
