package br.ucsal.bes20192.testequalidade.restaurante.builder;

import br.ucsal.bes20192.testequalidade.restaurante.domain.Item;

//NÃO UTILIZAR!!!
public class ItemObjectMother {

	public static Item umItemBarato() {
		Item item = new Item("bala", .5);
		return item;
	}

	public static Item umItemCaro() {
		Item item = new Item("Feijoada", 80.50);
		return item;
	}

}
