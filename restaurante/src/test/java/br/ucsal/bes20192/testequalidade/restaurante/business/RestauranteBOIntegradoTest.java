package br.ucsal.bes20192.testequalidade.restaurante.business;

import org.junit.jupiter.api.Test;

import br.ucsal.bes20192.testequalidade.restaurante.builder.ComandaObjectMother;
import br.ucsal.bes20192.testequalidade.restaurante.builder.ItemObjectMother;
import br.ucsal.bes20192.testequalidade.restaurante.builder.MesaBuilder;
import br.ucsal.bes20192.testequalidade.restaurante.builder.MesaObjectMother;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20192.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20192.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20192.testequalidade.restaurante.persistence.MesaDao;

public class RestauranteBOIntegradoTest {

	/**
	 * Método a ser testado: public void abrirComanda(Integer numeroMesa) throws
	 * RegistroNaoEncontrado, MesaOcupadaException. Verificar se a abertura de uma
	 * comanda para uma mesa livre apresenta sucesso. Caso de teste #1 Entrada é uma
	 * mesa livre Saída esperada é a comanda aberta Crie um builder para instanciar
	 * a classe Mesa.
	 * 
	 * @throws MesaOcupadaException
	 * @throws ComandaFechadaException
	 */
	@Test
	public void abrirComandaMesaLivre() throws MesaOcupadaException, ComandaFechadaException {
		// Definir dados de entrada
		// Criar uma instância da classe Mesa;
		// Mesa mesa1 = MesaObjectMother.umaMesaLivre();
		// MesaDao mesaDao = new MesaDao();
		// mesaDao.incluir(mesa1);

		Mesa mesa2 = MesaBuilder.umaMesaBuilder().build();
		MesaDao mesaDao = new MesaDao();
		mesaDao.incluir(mesa2);

		Mesa mesa3 = MesaBuilder.umaMesaBuilder().comNumero(45).comCapacidade(8).livre().build();

		MesaBuilder mesaBuilder1 = MesaBuilder.umaMesaBuilder().comNumero(45).comCapacidade(8).livre();
		Mesa mesa4 = mesaBuilder1.build();
		Mesa mesa5 = mesaBuilder1.mas().comNumero(56).build();
		Mesa mesa6 = mesaBuilder1.mas().comCapacidade(12).build();
		Mesa mesa7 = mesaBuilder1.mas().ocupada().build();
		Mesa mesa8 = mesaBuilder1.build();

		Mesa mesa9 = MesaBuilder.umaMesaLivreBuilder().comNumero(45).comCapacidade(8).build();

		System.out.println("mesa4=" + mesa4); // numero=45 ; capacidade=8 ; situacao=livre
		System.out.println("mesa5=" + mesa5); // numero=56 ; capacidade=8 ; situacao=livre
		System.out.println("mesa6=" + mesa6); // numero=45 ; capacidade=12 ; situacao=livre
		System.out.println("mesa7=" + mesa7); // numero=45 ; capacidade=8 ; situacao=ocupado
		System.out.println("mesa8=" + mesa8); // numero=45 ; capacidade=8 ; situacao=livre

		// Persistir a instância da classe Mesa na base, utilizando o MesaDAO;

		// Definir a saída esperada
		// Criar uma instância da classe Comanda que seria -> comandaEsperada;
		Comanda comanda = ComandaObjectMother.umaComandaCom2Itens();
		Item item2 = ItemObjectMother.umItemBarato();
		comanda.incluirItem(item2, 4);

		// Executar o método que está sendo testado e obter o resultado atual
		// Executar o método abrirComanda e obter a comanda atual

		// Comparar o resultado esperado com o resultado atual
		// Comparar a comandaEsperada com a comandaAtual;
	}

}
