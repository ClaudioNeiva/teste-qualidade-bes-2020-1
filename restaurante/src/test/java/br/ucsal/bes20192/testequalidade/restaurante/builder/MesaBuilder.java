package br.ucsal.bes20192.testequalidade.restaurante.builder;

import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20192.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class MesaBuilder {

	private static final SituacaoMesaEnum SITUACAO_DEFAULT = SituacaoMesaEnum.LIVRE;
	private static final Integer NUMERO_DEFAULT = 1;
	private static final Integer CAPACIDADE_DEFAULT = 4;

	private SituacaoMesaEnum situacao = SITUACAO_DEFAULT;
	private Integer numero = NUMERO_DEFAULT;
	private Integer capacidade = CAPACIDADE_DEFAULT;

	private MesaBuilder() {
	}

	public static MesaBuilder umaMesaBuilder() {
		return new MesaBuilder();
	}

	public static MesaBuilder umaMesaLivreBuilder() {
		return umaMesaBuilder().livre();
	}

	public static MesaBuilder umaMesaOcupadaBuilder() {
		return umaMesaBuilder().ocupada();
	}

	public MesaBuilder comNumero(Integer numero) {
		this.numero = numero;
		return this;
	}

	public MesaBuilder comCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
		return this;
	}

	public MesaBuilder comSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public MesaBuilder livre() {
		this.situacao = SituacaoMesaEnum.LIVRE;
		return this;
	}

	public MesaBuilder ocupada() {
		this.situacao = SituacaoMesaEnum.OCUPADA;
		return this;
	}

	public MesaBuilder mas() {
		return umaMesaBuilder().comCapacidade(capacidade).comNumero(numero).comSituacao(situacao);
	}

	public Mesa build() {
		Mesa mesa = new Mesa(numero);
		mesa.setCapacidade(capacidade);
		mesa.setSituacao(situacao);
		return mesa;
	}

}
