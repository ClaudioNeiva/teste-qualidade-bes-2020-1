package br.ucsal.bes20192.testequalidade.restaurante.builder;

import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20192.testequalidade.restaurante.enums.SituacaoMesaEnum;

// NÃO UTILIZAR!!!
public class MesaObjectMother {

	public static Mesa umaMesaLivre() {
		Integer numeroMesa = 1;
		Mesa mesa = new Mesa(numeroMesa);
		mesa.setSituacao(SituacaoMesaEnum.LIVRE);
		return mesa;
	}

	public static Mesa umaMesaOcupada() {
		Integer numeroMesa = 1;
		Mesa mesa = new Mesa(numeroMesa);
		mesa.setSituacao(SituacaoMesaEnum.OCUPADA);
		return mesa;
	}

}
