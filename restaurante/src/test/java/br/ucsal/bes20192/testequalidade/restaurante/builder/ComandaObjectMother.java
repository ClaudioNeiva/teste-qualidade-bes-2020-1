package br.ucsal.bes20192.testequalidade.restaurante.builder;

import br.ucsal.bes20192.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Item;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20192.testequalidade.restaurante.exception.ComandaFechadaException;
import br.ucsal.bes20192.testequalidade.restaurante.exception.MesaOcupadaException;

//NÃO UTILIZAR!!!
public class ComandaObjectMother {

	public static Comanda umaComandaCom2Itens() throws MesaOcupadaException, ComandaFechadaException {
		Mesa mesa = MesaObjectMother.umaMesaLivre();
		Comanda comanda = new Comanda(mesa);
		Item itemBala = ItemObjectMother.umItemBarato();
		Item itemFeijoada = ItemObjectMother.umItemCaro();
		comanda.incluirItem(itemBala, 5);
		comanda.incluirItem(itemFeijoada, 1);
		return comanda;
	}

	public static Comanda umaComandaCom3Itens() throws MesaOcupadaException, ComandaFechadaException {
		Comanda comanda = umaComandaCom2Itens();
		Item itemCaju = ItemObjectMother.umItemBarato();
		comanda.incluirItem(itemCaju, 2);
		return comanda;
	}

}
