package br.ucsal.bes20201.testequalidade.aula04;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculoUtilSaidaTest {

	private static final String QUEBRA_LINHA = System.getProperty("line.separator");

	private static PrintStream outOriginal;

	@BeforeClass
	public static void setupClass() {
		outOriginal = System.out;
	}

	@AfterClass
	public static void tearDownClass() {
		System.setOut(outOriginal);
	}

	@Test
	public void testarMensagemFatorial5() {
		int n = 5;
		long fat = 120;

		String mensagemEsperada = "Fatorial(5)=120" + QUEBRA_LINHA;

		// FIXME Gambi! Substituir pelo uso adequado de um framework de teste (Mockito?
		// , PowerMock? EasyMock?).
		ByteArrayOutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));

		CalculoUtil.exibirFatorial(n, fat);

		String mensagemAtual = outFake.toString();

		assertEquals(mensagemEsperada, mensagemAtual);
	}

}
