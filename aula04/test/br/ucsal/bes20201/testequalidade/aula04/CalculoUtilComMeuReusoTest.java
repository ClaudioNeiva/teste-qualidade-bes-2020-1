package br.ucsal.bes20201.testequalidade.aula04;

import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilComMeuReusoTest {

	@Test
	public void testarFatorial0() {
		testarFatorial(0, 1);
	}

	@Test
	public void testarFatorial3() {
		testarFatorial(3, 6);
	}

	@Test
	public void testarFatorial5() {
		testarFatorial(5, 120);
	}

	private void testarFatorial(int n, long fatorialEsperado) {
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
