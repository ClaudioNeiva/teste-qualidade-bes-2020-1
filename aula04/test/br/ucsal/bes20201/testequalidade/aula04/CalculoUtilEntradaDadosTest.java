package br.ucsal.bes20201.testequalidade.aula04;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class CalculoUtilEntradaDadosTest {

	private static InputStream inOriginal;

	@BeforeClass
	public static void setupClass() {
		inOriginal = System.in;
	}

	@AfterClass
	public static void tearDownClass() {
		System.setIn(inOriginal);
	}

	@Test
	public void testarEntrada10() {
		String n = "10";
		int saidaEsperada = 10;

		// FIXME Gambi! Substituir pelo uso adequado de um framework de teste (Mockito?
		// , PowerMock? EasyMock?).
		ByteArrayInputStream inFake = new ByteArrayInputStream(n.getBytes());
		System.setIn(inFake);

		int saidaAtual = CalculoUtil.obterNumero();

		assertEquals(saidaEsperada, saidaAtual);
	}

	@Test
	public void testarEntradaForaDaFaixa() {
		String n = "20\n-5\n8";
		int saidaEsperada = 8;

		// FIXME Gambi! Substituir pelo uso adequado de um framework de teste (Mockito?
		// , PowerMock? EasyMock?).
		ByteArrayInputStream inFake = new ByteArrayInputStream(n.getBytes());
		System.setIn(inFake);

		int saidaAtual = CalculoUtil.obterNumero();

		assertEquals(saidaEsperada, saidaAtual);

	}

}
