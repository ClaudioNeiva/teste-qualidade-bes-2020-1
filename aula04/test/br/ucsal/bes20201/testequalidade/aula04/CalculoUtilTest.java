package br.ucsal.bes20201.testequalidade.aula04;

//import static org.junit.Assert.*;
import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilTest {

	// Processo dentro de cada m�todo de teste:
	// 1. Definir dados de entrada;
	// 2. Definir a sa�da esperada;
	// 3. Executar a m�todo a ser testado e obter a sa�da atual;
	// 4. Comparar o resultado esperado com o resultado atual.

	@Test
	public void testarFatorial0() {
		int n = 0;
		long fatorialEsperado = 1;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
//		assertEquals(fatorialEsperado, fatorialAtual);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial3() {
		int n = 3;
		long fatorialEsperado = 6;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial5() {
		int n = 5;
		long fatorialEsperado = 120;
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarFatorial0E3E5() {
		// N�O FA�A!!!!
		Assert.assertEquals(1, CalculoUtil.calcularFatorial(0));
		Assert.assertEquals(6, CalculoUtil.calcularFatorial(3));
		Assert.assertEquals(120, CalculoUtil.calcularFatorial(5));
		
	}
	
}
