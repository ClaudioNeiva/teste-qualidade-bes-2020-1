package br.ucsal.bes20201.testequalidade.aula04;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CalculoUtilParametrizedTest {

	@Parameters(name = "{index} - calcularFatorial({0})")
	public static Collection<Object[]> carregarCasosTeste() {
		return Arrays.asList(new Object[][] { { 0, 1 }, { 1, 1 }, { 3, 6 }, { 5, 120 } });
	}

	// @Parameters(name = "{index} - calcularFatorial({0})")
	// public static Collection<Object[]> carregarCasosTeste(){
	// List<Object[]> casosTeste = new ArrayList<>();
	// casosTeste.add(new Object[] {0, 1});
	// casosTeste.add(new Object[] {1, 1});
	// casosTeste.add(new Object[] {3, 6});
	// casosTeste.add(new Object[] {5, 120});
	// return casosTeste;
	// }

	@Parameter(0)
	public int n;

	@Parameter(1)
	public long fatorialEsperado;

	@Test
	public void testarFatorial() {
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
