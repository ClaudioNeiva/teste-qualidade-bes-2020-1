package br.ucsal.bes20201.testequalidade.aula04;

public class CalculoUtilSemFrameworkTest {

	public static void main(String[] args) {
		testarFatorial(0, 1);
		testarFatorial(3, 6);
		testarFatorial(5, 120);
	}

	private static void testarFatorial(int n, long fatorialEsperado) {
		try {
			long fatorialAtual = CalculoUtil.calcularFatorial(n);
			if (fatorialEsperado == fatorialAtual) {
				System.out.println("calcularFatorial(" + n + "). Sucesso.");
			} else {
				System.out.println("calcularFatorial(" + n + "). Falha: Esperado=" + fatorialEsperado + " mas atual="
						+ fatorialAtual);
			}
		} catch (Exception e) {
			System.out.println("calcularFatorial(" + n + "). Erro." + e.getMessage());
		}
	}

}
