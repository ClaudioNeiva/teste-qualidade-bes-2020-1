package br.ucsal.bes20201.testequalidade.aula11;

public class CalculoEHelper {
	
	private FatorialHelper fatorialHelper;

	public CalculoEHelper() {
	}

	public CalculoEHelper(FatorialHelper fatorialHelper) {
		this.fatorialHelper = fatorialHelper;
	}

	public Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / fatorialHelper.calcularFatorial(i);
		}
		return e;
	}

}
