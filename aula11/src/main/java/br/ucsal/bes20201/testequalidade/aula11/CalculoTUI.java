package br.ucsal.bes20201.testequalidade.aula11;

public class CalculoTUI {

	public static void main(String[] args) {
		FatorialHelper fatorialHelper = new FatorialHelper();
		CalculoEHelper calculoEHelper = new CalculoEHelper(fatorialHelper);
		CalculoTUI calculoTUI = new CalculoTUI();
		Integer n = 3;
		Double e = calculoEHelper.calcularE(n);
		calculoTUI.exibirE(n, e);
	}

	public void exibirE(Integer n, Double e) {
		System.out.println("E(" + n + ")=" + e);
	}

}
