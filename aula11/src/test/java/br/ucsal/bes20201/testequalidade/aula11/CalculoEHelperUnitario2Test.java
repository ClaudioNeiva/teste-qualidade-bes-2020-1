package br.ucsal.bes20201.testequalidade.aula11;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class CalculoEHelperUnitario2Test {

	@Mock
	private FatorialHelper fatorialHelperMock;

	@InjectMocks
	private CalculoEHelper calculoEHelper;

	@BeforeEach
	public void setup() {
		initMocks(this);
	}

	@Test
	public void testarE0() {
		Integer n = 0;
		Double eEsperado = 1d;

		when(fatorialHelperMock.calcularFatorial(0)).thenReturn(1L);

		Double eAtual = calculoEHelper.calcularE(n);

		/*
		 * O método calcularE(n) é do tipo QUERY, por isto, não preciso fazer verify!
		 * Para verificar o sucesso do calcularE(n), basta comparar o valor retornado
		 * com um valor esperado.
		 */
		Assertions.assertEquals(eEsperado, eAtual);

	}

	public void testarE2() {
		Integer n = 2;
		Double eEsperado = 2.5d;
	}

}
