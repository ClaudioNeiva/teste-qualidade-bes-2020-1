package br.ucsal.bes20201.testequalidade.aula11;

import java.io.PrintStream;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CalculoTuiTest {

	@Test
	public void testarExibirEN3() {

		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		System.setOut(printStreamMock);

		CalculoTUI calculoTUI = new CalculoTUI();
		Integer n = 2;
		Double e = 2.5d;
		calculoTUI.exibirE(n, e);

		/*
		 * O método exibirE é do tipo COMMAND. Para saber se esse método foi executado
		 * com sucesso, devo verificar se ocorreu uma chamada, com valores específicos
		 * de parâmetro para o System.out.println.
		 */
		Mockito.verify(printStreamMock).println("E(2)=2.5");
	}

}
