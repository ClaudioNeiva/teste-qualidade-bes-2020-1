package br.ucsal.bes20201.testequalidade.aula12;

import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitPlatform.class)
@PrepareForTest({ Classe1.class })
public class Classe1Test {

	@Test
	public void testarMetodo4() throws Exception {
		Classe1 objeto1 = new Classe1();
		Classe1 spy1 = spy(objeto1);

		mockStatic(Classe1.class);
		when(Classe1.metodo1()).thenReturn(10);
		when(Classe1.class, "metodo2").thenReturn(20);
		when(spy1, "metodo3").thenReturn(30);

		spy1.metodo4();

		verifyPrivate(Classe1.class).invoke("metodo2");
		verifyPrivate(spy1).invoke("metodo3");

		verifyStatic(Classe1.class);
		Classe1.metodo1();

		Assertions.assertEquals(1,1);
		
		verifyNoMoreInteractions(Classe1.class);
	}
}
