package br.ucsal.bes20201.testequalidade.aula06.business;

public class CalculoBO {

	public Long calcularFatorial(Integer n) {
		Long fatorial = 1L;
		for (int i = 1; i < n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

}
