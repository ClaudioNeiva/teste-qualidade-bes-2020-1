package br.ucsal.bes20201.testequalidade.aula06.tui;

import br.ucsal.bes20201.testequalidade.aula06.business.CalculoBO;

public class CalculadoraTUI {

	private CalculoBO calculoBO;

	public static void main(String[] args) {
		CalculadoraTUI calculoadoraTUI = new CalculadoraTUI();
		calculoadoraTUI.calcularApresentarFatorial(5);
	}

	public CalculadoraTUI() {
		calculoBO = new CalculoBO();
	}

	public void calcularApresentarFatorial(Integer n) {
		Long fatorial;
		fatorial = calculoBO.calcularFatorial(n);
		System.out.println("fatorial(" + n + ")=" + fatorial);
	}

}
