package br.ucsal.bes20201.testequalidade.aula10.diversos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.aula10.diversos.CalculoUtil;

public class CalculoUtilTest {

	@Test
	public void testarE0() {

		Integer n = 0;

		Double eEsperado = 1d;

		Double eAtual = CalculoUtil.calcularE(n);

		Assertions.assertEquals(eEsperado, eAtual);

	}

	@Test
	public void testarE1() {

		Integer n = 1;

		Double eEsperado = 2d;

		Double eAtual = CalculoUtil.calcularE(n);

		Assertions.assertEquals(eEsperado, eAtual);

	}

}
