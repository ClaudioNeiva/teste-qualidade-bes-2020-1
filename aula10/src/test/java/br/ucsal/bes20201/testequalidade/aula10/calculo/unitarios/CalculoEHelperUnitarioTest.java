package br.ucsal.bes20201.testequalidade.aula10.calculo.unitarios;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.aula10.calculo.CalculoEHelper;
import br.ucsal.bes20201.testequalidade.aula10.calculo.FatorialHelper;

public class CalculoEHelperUnitarioTest {

	private static CalculoEHelper calculoEHelper;

	@BeforeAll
	public static void setupClass() {
		/*
		 * FIXME Para que o teste seja UNITÁRIO, e necessário resolver a dependência da
		 * execução do método FatorialHelper.calcularFatorial. Para isso, teremos que
		 * construir um dublê de código!!!
		 * Implementar um solução que responda as chamadas calcularFatorial de forma adequada e FIXA.
		 * calcularFatorial(0) -> 1 
		 * calcularFatorial(1) -> 1 
		 * calcularFatorial(2) -> 2 
		 */
		FatorialHelper fatorialHelper = new FatorialHelperStub();
		calculoEHelper = new CalculoEHelper(fatorialHelper);
	}

	@Test
	public void testarE0() {
		Integer n = 0;
		Double eEsperado = 1d;

		Double eAtual = calculoEHelper.calcularE(n);

		Assertions.assertEquals(eEsperado, eAtual);
	}

	@Test
	public void testarE2() {
		Integer n = 2;
		Double eEsperado = 2.5d;

		Double eAtual = calculoEHelper.calcularE(n);

		Assertions.assertEquals(eEsperado, eAtual);
	}

}
