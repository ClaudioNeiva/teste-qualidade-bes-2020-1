package br.ucsal.bes20201.testequalidade.aula10.diversos;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.aula10.diversos.DiversosUtil;

public class DiversosUtilTest {

	@Test
	// 1 - 2 - 4 - 6
	public void testar15e1() {
		int x = 15;
		int y = 1;

		Integer algumaCoisaAtual = DiversosUtil.executarAlgumaCoisa(x, y);

		// FIXME Implementar o assert!		
	}

	@Test
	// 1 - 2 - 4 - 5 - 6
	public void testar35e1() {
		int x = 35;
		int y = 1;

		Integer algumaCoisaAtual = DiversosUtil.executarAlgumaCoisa(x, y);

		// FIXME Implementar o assert!		
	}

	@Test
	// 1 - 2 - 4 - 5 - 6
	public void testar15e9() {
		int x = 15;
		int y = 9;

		Integer algumaCoisaAtual = DiversosUtil.executarAlgumaCoisa(x, y);

		// FIXME Implementar o assert!		
	}

	@Test
	// 1 - 3 - 4 - 6
	public void testar2e5() {
		int x = 2;
		int y = 5;

		Integer algumaCoisaAtual = DiversosUtil.executarAlgumaCoisa(x, y);

		// FIXME Implementar o assert!		
	}

	
	@Test
	// 1 - 3 - 4 - 5 - 6
	public void testar2e15() {
		int x = 2;
		int y = 15;

		Integer algumaCoisaAtual = DiversosUtil.executarAlgumaCoisa(x, y);

		// FIXME Implementar o assert!		
	}

}
