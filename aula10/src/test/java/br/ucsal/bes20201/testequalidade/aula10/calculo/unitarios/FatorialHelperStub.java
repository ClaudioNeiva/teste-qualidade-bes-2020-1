package br.ucsal.bes20201.testequalidade.aula10.calculo.unitarios;

import br.ucsal.bes20201.testequalidade.aula10.calculo.FatorialHelper;

public class FatorialHelperStub extends FatorialHelper {

	@Override
	public Long calcularFatorial(Integer n) {
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		default:
			throw new RuntimeException("Fatorial(" + n + ") não configurado.");
		}
	}

}
