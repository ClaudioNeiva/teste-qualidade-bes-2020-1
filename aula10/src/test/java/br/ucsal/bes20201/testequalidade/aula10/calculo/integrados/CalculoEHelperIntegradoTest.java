package br.ucsal.bes20201.testequalidade.aula10.calculo.integrados;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.aula10.calculo.CalculoEHelper;
import br.ucsal.bes20201.testequalidade.aula10.calculo.FatorialHelper;

public class CalculoEHelperIntegradoTest {

	private static CalculoEHelper calculoEHelper;

	@BeforeAll
	public static void setupClass() {
		FatorialHelper fatorialHelper = new FatorialHelper();
		calculoEHelper = new CalculoEHelper(fatorialHelper);
	}

	@Test
	public void testarE0() {
		Integer n = 0;
		Double eEsperado = 1d;

		Double eAtual = calculoEHelper.calcularE(n);

		Assertions.assertEquals(eEsperado, eAtual);
	}

	@Test
	public void testarE2() {
		Integer n = 2;
		Double eEsperado = 2.5d;

		Double eAtual = calculoEHelper.calcularE(n);

		Assertions.assertEquals(eEsperado, eAtual);
	}

}
