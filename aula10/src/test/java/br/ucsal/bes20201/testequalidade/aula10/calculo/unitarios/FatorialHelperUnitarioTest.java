package br.ucsal.bes20201.testequalidade.aula10.calculo.unitarios;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.aula10.calculo.FatorialHelper;

public class FatorialHelperUnitarioTest {

	private static FatorialHelper fatorialHelper;

	@BeforeAll
	public static void setupClass() {
		fatorialHelper = new FatorialHelper();
	}

	@Test
	public void testarFatorial3() {
		Integer n = 3;
		Long fatorialEsperado = 6L;
		Long fatorialAtual = fatorialHelper.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
