package br.ucsal.bes20201.testequalidade.aula10.diversos;

public class CalculoUtil {

	public static Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			Long fat = 1L;
			for (int j = 1; j <= i; j++) {
				fat *= j;
			}
			e += 1d / fat;
		}
		return e;
	}

}
