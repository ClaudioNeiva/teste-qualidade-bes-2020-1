package br.ucsal.bes20201.testequalidade.aula10.calculo;

public class FatorialHelper {

	/*
	 * FIXME Existe um erro aqui, que está sendo apontado como erro na classe
	 * CalculoEHelpIntegradoTest
	 */
	public Long calcularFatorial(Integer n) {
		Long fat = 1L;
		for (int i = 1; i < n; i++) {
			fat *= i;
		}
		return fat;
	}

}
