package br.ucsal.bes20201.testequalidade.aula10.diversos;

public class DiversosUtil {

	public static Integer executarAlgumaCoisa(int x, int y) {

		Integer a = 2 * x;
		Integer b = null;
		Integer f = 1;

		if (x > 10) {

			a = 3 * a;
			b = 5 * y;
			f = 5;

		} else {
			
			f = 0;
			b = 2;
			
		}

		if (y > 8 || x > 30) {

			b = 2 * a;
			a = b / f;

		}

		Integer c = 2 * a;
		Integer d = 3 * b + c;

		return d;

	}

}
