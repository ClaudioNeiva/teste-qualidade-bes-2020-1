package br.ucsal.bes20201.testequalidade.caderneta20201.business;

public class CalculoBO {

	// FIXME Este código tem um problema de implementação do requisito. Ele será
	// utilizado para ilustrar testes que revelarão o problema.
	public static ConceitoEnum definirConceito(Double media) throws MediaForaFaixaException {
		ConceitoEnum conceito;
		if (media < 3) {
			conceito = ConceitoEnum.REPROVADO;
		} else if (media <= 6) {
			conceito = ConceitoEnum.PROVA_FINAL;
		} else if (media < 10) {
			conceito = ConceitoEnum.APROVADO;
		} else {
			throw new MediaForaFaixaException(media);
		}
		return conceito;
	}

}
