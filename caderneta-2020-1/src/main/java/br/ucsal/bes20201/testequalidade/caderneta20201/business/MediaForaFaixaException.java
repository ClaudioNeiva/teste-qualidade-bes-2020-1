package br.ucsal.bes20201.testequalidade.caderneta20201.business;

public class MediaForaFaixaException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final String MEDIA_FORA_FAIXA = "Média fora da faixa (0 a 10): %.1f";

	public MediaForaFaixaException(Double media) {
		super(String.format(MEDIA_FORA_FAIXA, media));
	}
	
}
