package br.ucsal.bes20201.testequalidade.caderneta20201.business.unitario;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.caderneta20201.business.CalculoBO;
import br.ucsal.bes20201.testequalidade.caderneta20201.business.MediaForaFaixaException;

public class CalculoBOTest {

	// No JUnit 4
	// @Test(except = MediaForaFaixaException.class)
	// public void testarDefinicaoConceitoMedia20() {
	// Double media = 20.0;
	// CalculoBO.definirConceito(media);
	// }

	@Test
	@DisplayName("Testar a tentativa definição de conceito para uma média (20.00), superior ao limite.")
	public void testarDefinicaoConceitoMedia20() {
		Double media = 20.0;
		assertThrows(MediaForaFaixaException.class, () -> {
			CalculoBO.definirConceito(media);
		});
	}

	@Test
	@DisplayName("Testar a tentativa definição de conceito para uma média (20.00), superior ao limite, verificando a mensagem.")
	public void testarDefinicaoConceitoMedia20Mensagem() {
		Double media = 20.0;
		String mensagemEsperada = "Média fora da faixa (0 a 10): 20,0";
		Exception exception = assertThrows(MediaForaFaixaException.class, () -> {
			CalculoBO.definirConceito(media);
		});
		assertEquals(mensagemEsperada, exception.getMessage());
	}

	@Test
	@DisplayName("Testar a tentativa definição de conceito para uma média (-5.00), inferior ao limite.")
	public void testarDefinicaoConceitoMediaMenos5() {
		Double media = -5.0;
		assertThrows(MediaForaFaixaException.class, () -> {
			CalculoBO.definirConceito(media);
		});
	}

}
