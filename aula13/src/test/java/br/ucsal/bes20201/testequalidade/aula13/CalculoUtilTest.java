package br.ucsal.bes20201.testequalidade.aula13;

import org.junit.Assert;
import org.junit.Test;

public class CalculoUtilTest {

	// TDD Test Driven Development - Desenvolvimento Dirigido à Teste

	/**
	 * Caso de teste1:
	 * 
	 * Entrada: 5
	 * 
	 * Saída esperada: 120
	 */
	@Test
	public void testarFatorial5() {
		Integer n = 5;
		Long fatorialEsperado = 120L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
