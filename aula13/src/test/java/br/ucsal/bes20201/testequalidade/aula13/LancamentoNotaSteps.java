package br.ucsal.bes20201.testequalidade.aula13;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class LancamentoNotaSteps {

	private Aluno aluno;

	@Given("um aluno está matriculado na disciplina")
	public void instanciarAluno() {
		// Comportamento de teste
		aluno = new Aluno();
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		// Comportamento de teste
		aluno.informarNota(nota);
	}

	@Then("a situação do aluno é $situacao")
	public void verificarSituacaoAluno(String situacao) {
		// Verificação com Assert
		Assert.assertEquals(situacao, aluno.obterSituacao());
	}

}
