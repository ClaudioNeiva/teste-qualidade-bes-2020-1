package br.ucsal.bes20201.testequalidade.aula13;

public class CalculoUtil {

	public static Long calcularFatorial(Integer n) {
		if (n == 0) {
			return 1L;
		}
		return n * calcularFatorial(n - 1);
	}

}
